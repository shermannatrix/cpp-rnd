#include <iostream>
#include <limits>

template<typename T>
void printMinMax(const char* type) {
	std::cout << type;
	std::cout << ", min: " << std::numeric_limits<T>::min();
	std::cout << ", max: " << std::numeric_limits<T>::max() << std::endl;
}

template<typename T>
void printCharMinMax(const char* type) {
	std::cout << type;
	std::cout << ", min: " << (int) std::numeric_limits<T>::min();
	std::cout << ", max: " << (int) std::numeric_limits<T>::max() << std::endl;
}

int main() {
	printMinMax<bool>("bool");
	printCharMinMax<char>("char");
	printCharMinMax<unsigned char>("unsigned char");
	printMinMax<wchar_t>("wchar_t");

	printMinMax<short>("short");
	printMinMax<unsigned short>("unsigned short");
	printMinMax<int>("int");
	printMinMax<unsigned int>("unsigned int");
	printMinMax<long>("long");
	printMinMax<unsigned long>("unsigned long");
	printMinMax<long long>("long long");
	printMinMax<unsigned long long>("unsigned long long");
	printMinMax<float>("float");
	printMinMax<double>("double");
	printMinMax<long double>("long double");

	return 0;
}